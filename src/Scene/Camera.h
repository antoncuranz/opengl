#ifndef CAMERA_H
#define CAMERA_H


#include <glm/glm.hpp>

class Camera {
public:
    Camera(float fov, float aspect, float near, float far);
    Camera() : Camera(45.0f, 800.0f/600.0f, 0.01f, 1000.0f) {};
    void lookAt(glm::vec3 target);
    void eye(glm::vec3 eye);
    void eye(float x, float y, float z);
    void aspect(float aspect);
    glm::mat4 pvMatrix();
    glm::mat4 m_projection, m_view;
    glm::vec3 m_eye;
private:
    glm::vec3 m_up, m_target;
    float m_fov, m_aspect, m_near, m_far;
};


#endif //CAMERA_H
