#ifndef SCENECONTROLLER_H
#define SCENECONTROLLER_H


#include "AbstractSceneController.h"
#include "../Mesh/Mesh.h"
#include "../Core/Texture.h"
#include "../Core/Material.h"

class SceneController : AbstractSceneController {
public:
    SceneController();
    void preRender(float delta);
    inline void render() { renderer.render(scene, camera); };
private:
    std::vector<std::shared_ptr<Shader>>   shaders;
    std::vector<std::shared_ptr<Material>> materials;
	std::vector<std::shared_ptr<Mesh>>     meshes;
	glm::vec3 light;
//	Texture *texture;
};

#endif //SCENECONTROLLER_H
