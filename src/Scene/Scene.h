#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <optional>
#include <glm/glm.hpp>
#include "../Core/VertexArray.h"
#include "../Core/IndexBuffer.h"
#include "../Core/Shader.h"
#include "../Core/Material.h"

class SceneObject {
public:
    SceneObject();
    void translate(glm::vec3 translation);
    void rotate(float angle, glm::vec3 axis);
    void add(std::shared_ptr<SceneObject> child);
    void remove(std::shared_ptr<SceneObject> child);
    void setMaterial(std::shared_ptr<Material> material);

    virtual void draw();

    VertexArray va;
    std::optional<IndexBuffer> ib;
    glm::mat4 model;
    std::vector<std::shared_ptr<SceneObject>> children;
    std::shared_ptr<Material> m_Material;
    char renderType;
//protected:
    std::optional<VertexBuffer> vb;
    VertexBufferLayout layout;
};


class Scene {
public:
    Scene();
    void add(std::shared_ptr<SceneObject> child);
    void remove(std::shared_ptr<SceneObject> child);

    SceneObject root;
};

#endif //SCENE_H
