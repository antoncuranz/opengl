#include <glm/ext/matrix_transform.hpp>
#include "Scene.h"

SceneObject::SceneObject() : m_Material(nullptr), model(1.0), renderType(GL_TRIANGLES) { }

void SceneObject::setMaterial(std::shared_ptr<Material> material) {
    m_Material = std::move(material);
}

void SceneObject::draw() {
    va.bind();
    ib->bind();
    m_Material->bind();
    glDrawElements(renderType, ib->getCount(), GL_UNSIGNED_INT, (void*)0);
}

void SceneObject::add(std::shared_ptr<SceneObject> child) {
    children.push_back(std::move(child));
}

void SceneObject::remove(std::shared_ptr<SceneObject> child) {
    // TODO: make this work.
    // children.erase(std::remove(children.begin(), children.end(), child), children.end());
}
void SceneObject::translate(glm::vec3 translation) {
    model = glm::translate(model, translation);
}

void SceneObject::rotate(float angle, glm::vec3 axis) {
    model = glm::rotate(model, angle, axis);
}


Scene::Scene() = default;

void Scene::add(std::shared_ptr<SceneObject> child) {
    root.add(std::move(child));
}

void Scene::remove(std::shared_ptr<SceneObject> child) {
    //TODO: make this recursive.
    root.remove(std::move(child));
}
