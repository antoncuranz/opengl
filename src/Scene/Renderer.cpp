#include "Renderer.h"
#include <glm/gtc/type_ptr.hpp>

Renderer::Renderer() = default;

void Renderer::render(Scene &scene, Camera &camera) {
    glm::mat4 model(1.0);
    recur(std::make_shared<SceneObject>(scene.root), camera, model);
}

void Renderer::recur(const std::shared_ptr<SceneObject>& object, Camera &camera, glm::mat4 model) {
    // update matrices
    model = model * object->model;
    glm::mat4 mvit = glm::transpose(glm::inverse(camera.m_view * model));


    // draw current object
    if(object->m_Material != nullptr) {
        auto shader = object->m_Material->m_Shader;

        shader->bind();

        // TODO: check UniformFlags
        Uniform mvitu { UDT_Mat4, glm::value_ptr(mvit) };
        shader->setUniform(mvitu, "mvit");

        Uniform modelu { UDT_Mat4, glm::value_ptr(model) };
        shader->setUniform(modelu, "model");

        Uniform viewu { UDT_Mat4, glm::value_ptr(camera.m_view) };
        shader->setUniform(viewu, "view");

        Uniform projectionu { UDT_Mat4, glm::value_ptr(camera.m_projection) };
        shader->setUniform(projectionu, "projection");

        Uniform eyeu { UDT_Mat4, glm::value_ptr(camera.m_eye) };
        shader->setUniform(eyeu, "eye");

        object->draw();
    }

    // draw children
    for(const auto& child: object->children) {
        recur(child, camera, model);
    }
}