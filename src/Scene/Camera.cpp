#include <glm/gtc/matrix_transform.hpp>
#include "Camera.h"

Camera::Camera(float fov, float aspect, float near, float far)
: m_projection(glm::perspective(glm::radians(fov), aspect, near, far)),
  m_fov(fov), m_aspect(aspect), m_near(near), m_far(far), m_eye(0.0),
  m_up(0.0, 1.0, 0.0), m_target(0.0), m_view(glm::lookAt(m_eye, m_target, m_up))
{
}

void Camera::lookAt(glm::vec3 target) {
    m_target = target;
    m_view = glm::lookAt(m_eye, m_target, m_up);
}

void Camera::eye(glm::vec3 eye) {
    m_eye = eye;
    m_view = glm::lookAt(m_eye, m_target, m_up);
}

void Camera::eye(float x, float y, float z) {
    eye(glm::vec3(x, y, z));
}

void Camera::aspect(float aspect) {
    m_aspect = aspect;
    m_projection = glm::perspective(glm::radians(m_fov), m_aspect, m_near, m_far);
}

glm::mat4 Camera::pvMatrix() {
    return m_projection * m_view;
}
