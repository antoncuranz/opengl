#include <glm/gtc/type_ptr.hpp>
#include "SceneController.h"
#include "../Mesh/Mesh.h"
#include "../Mesh/Sphere.h"
#include "../Mesh/Grid2D.h"
#include "../Mesh/ModelLoader.h"

SceneController::SceneController() : light(0.5f, 1.0f, -1.0f) {
    auto *texture = new Texture("../resources/obj_data/sponza/textures/sponza_curtain_diff.tga");

    // --- Materials ---
    auto phong = std::make_shared<Shader>("../resources/shaders/phong.vert", "../resources/shaders/phong.frag");
    auto Phong = std::make_shared<Material>(phong, UF_PHONG);

    auto simple = std::make_shared<Shader>("../resources/shaders/simple.vert", "../resources/shaders/simple.frag");
    auto Simple = std::make_shared<Material>(simple, UF_MVP);

    auto textured = std::make_shared<Shader>("../resources/shaders/texture.vert", "../resources/shaders/texture.frag");
    auto Textured = std::make_shared<Material>(textured, UF_MVP);

    auto color = new glm::vec4(0.7, 0.2, 0.2, 1.0);

    Phong->addUniform("light", Uniform { UDT_Vec3, glm::value_ptr(light) });
    Phong->addUniform("Kd", Uniform { UDT_Vec4, glm::value_ptr(*color) });

//    Simple->addUniform("color", Uniform { UDT_Vec4, glm::value_ptr(*color) });

    Textured->addUniform("texture", Uniform { UDT_Sampler2D, texture} );

    // --- Meshes ---
    std::vector<std::shared_ptr<Mesh>> sponza_meshes;
    sponza_meshes = ModelLoader::loadOBJ("../resources/obj_data/sponza/sponza.obj");

    for(auto &mesh: sponza_meshes) {
        scene.add(mesh);
    }

    auto hand   = std::make_shared<Mesh>("../resources/obj_data/hand_200.obj");
    auto grid   = std::make_shared<Grid2D>(glm::vec3(0.0, -0.5, 0.0), 4.0, 4.0, 12);

    hand->setMaterial(Phong);
    grid->setMaterial(Simple);

    scene.add(hand);
    scene.add(grid);
}

void SceneController::preRender(float delta) {
    camera.eye(sin(xi) *sin(yi)* radius, cos(yi) * radius, cos(xi) * sin(yi) * radius);
//    meshes[0]->rotate(delta * glm::radians(50.0f), glm::vec3(0.5f, 1.0f, 0.0f));
}
