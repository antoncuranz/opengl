#ifndef RENDERER_H
#define RENDERER_H

#include <vector>
#include "Camera.h"
#include "Scene.h"

class Renderer {
public:
    Renderer();
    void render(Scene &scene, Camera &camera);
private:
    void recur(const std::shared_ptr<SceneObject>& object, Camera &camera, glm::mat4 model);
};

#endif //RENDERER_H
