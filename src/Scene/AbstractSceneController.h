#ifndef ABSTRACTSCENECONTROLLER_H
#define ABSTRACTSCENECONTROLLER_H


#include "Scene.h"
#include "Camera.h"
#include "Renderer.h"

class AbstractSceneController {
public:
    AbstractSceneController();
    virtual void handleMouseClick(int button, int action, int mods);
    virtual void handleMouseMove(double xpos, double ypos);
    virtual void handleScroll(double xoffset, double yoffset);
    virtual void handleWindowResize(int width, int height);

protected:
    Renderer renderer;
    Camera camera;
    Scene scene;
    float radius, lastxpos, lastypos, xi, yi;
    bool orbitEnabled, panEnabled;
};


#endif //ABSTRACTSCENECONTROLLER_H
