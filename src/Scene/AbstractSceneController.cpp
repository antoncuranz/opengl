#include <glm/gtc/matrix_transform.hpp>
#include "AbstractSceneController.h"

AbstractSceneController::AbstractSceneController()
: radius(3.0), xi(1.0), yi(1.0), lastxpos(0.0), lastypos(0.0), orbitEnabled(false), panEnabled(false) {}

void AbstractSceneController::handleMouseClick(int button, int action, int mods) {
    if(action == 1 /*GLFW_PRESS*/) {
        if (button == 0 /*GLFW_MOUSE_BUTTON_LEFT*/)
            orbitEnabled = true;
        if (button == 1 /*GLFW_MOUSE_BUTTON_RIGHT*/)
            panEnabled = true;
    } else if(action == 0 /*GLFW_RELEASE*/) {
        orbitEnabled = false;
        panEnabled = false;
    }
}

void AbstractSceneController::handleMouseMove(double xpos, double ypos) {
    if (orbitEnabled) {
        xi += (lastxpos-xpos)/100.0;
        yi = glm::clamp(yi + (lastypos-ypos)/100.0, 0.01, 3.14);
    } else if (panEnabled) {
        camera.m_projection = glm::translate(camera.m_projection, glm::vec3(-(lastxpos-xpos)/500.0, (lastypos-ypos)/500.0, 0.0));
    }
    lastxpos = xpos;
    lastypos = ypos;
}

void AbstractSceneController::handleScroll(double xoffset, double yoffset) {
    radius = glm::max(0.0, radius + yoffset * 0.1);
}

void AbstractSceneController::handleWindowResize(int width, int height) {
    camera.m_projection = glm::perspective(glm::radians(45.0f), (float) width / height, 0.01f, 1000.0f);
}

