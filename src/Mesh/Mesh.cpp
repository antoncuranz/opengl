#include <iostream>
#include <fstream>
#include <regex>
#include <glm/gtx/string_cast.hpp>
#include <glad/glad.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Mesh.h"

using namespace std;

Mesh::Mesh(const string& path) {

    Assimp::Importer importer;

    const aiScene* pScene = importer.ReadFile(path.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_JoinIdenticalVertices);

    cout << "[Assimp] File contains " << pScene->mNumMeshes << " meshes and " << pScene->mNumMaterials << " materials." << endl;

	aiMesh *firstMesh = pScene->mMeshes[0]; //TODO

	for(int i = 0; i < firstMesh->mNumVertices; i++) {
		aiVector3D *pPos = &(firstMesh->mVertices[i]);
		aiVector3D *pNormal = &(firstMesh->mNormals[i]);
		aiVector3D *pTexCoord = &(firstMesh->mTextureCoords[0][i]);

		vertices.emplace_back(pPos->x, pPos->y, pPos->z);
		normals.emplace_back(pNormal->x, pNormal->y, pNormal->z);
		texCoords.emplace_back(pTexCoord->x, pTexCoord->y);
	}

	for(int i = 0; i < firstMesh->mNumFaces ; i++) {
		aiFace& face = firstMesh->mFaces[i];
		faces.push_back(face.mIndices[0]);
		faces.push_back(face.mIndices[1]);
		faces.push_back(face.mIndices[2]);
	}

    this->calculateFaceNormals();
    this->calculateVertexNormals(false);
    this->calculateVertexArray();

    vb.emplace(&vertexArray[0], vertices.size()*8*sizeof(float));
    ib.emplace(&faces[0], faces.size());

    // vertex attributes
    layout.push<float>(3); // position
    layout.push<float>(3); // normal
	layout.push<float>(2); // texCoords

    va.addBuffer(*vb, layout);
}

void Mesh::calculateVertexNormals(bool weighted) {

    // initialize arrays
    for(glm::vec3 vertex: this->vertices) {
        this->normals.emplace_back(0.0f, 0.0f, 0.0f);
    }

    // add face normals
    for(int i = 0; i < this->faces.size(); i ++) {
        this->normals[this->faces[i]] += this->faceNormals[i/3];
    }

    // normalize normals :-)
    for(glm::vec3 &normal: this->normals) {
        normal = glm::normalize(normal);
    }
}

void Mesh::calculateFaceNormals() {
    this->faceNormals = vector<glm::vec3>();

    for(int i = 0; i < this->faces.size(); i += 3) {
        vector<glm::vec3> faceVertices;
        transform(this->faces.begin()+i, this->faces.begin()+i+3, back_inserter(faceVertices),
                [this](int faceIndex) { return this->vertices[faceIndex]; });

        glm::vec3 U = faceVertices[1] - faceVertices[0];
        glm::vec3 V = faceVertices[2] - faceVertices[0];

        float nx = U.y * V.z - U.z * V.y;
        float ny = U.z * V.x - U.x * V.z;
        float nz = U.x * V.y - U.y * V.x;

        glm::vec3 normal = glm::normalize(glm::vec3(nx, ny, nz));
        this->faceNormals.push_back(normal);
    }
}

void Mesh::calculateVertexArray() {

    for(int i = 0; i < this->vertices.size(); i++) {
        glm::vec3 vertex = this->vertices[i];
        glm::vec3 normal = this->normals[i];
        glm::vec2 texCoord = this->texCoords[i];

        vertexArray.push_back(vertex.x);
        vertexArray.push_back(vertex.y);
        vertexArray.push_back(vertex.z);
        vertexArray.push_back(normal.x);
        vertexArray.push_back(normal.y);
        vertexArray.push_back(normal.z);
        vertexArray.push_back(texCoord.x);
        vertexArray.push_back(texCoord.y);
    }
}

Mesh::Mesh() = default;

