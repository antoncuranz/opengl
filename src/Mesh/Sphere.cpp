#include <glm/gtx/string_cast.hpp>
#include "Sphere.h"

Sphere::Sphere(glm::vec3 position, float radius, int subdivisions) : Mesh() {

    // initialize vertices/faces with basic octahedron
    vertices = std::vector<glm::vec3>{
        glm::vec3(0.0, 1.0, 0.0),
        glm::vec3(1.0, 0.0, 1.0),
        glm::vec3(1.0, 0.0, -1.0),
        glm::vec3(-1.0, 0.0, -1.0),
        glm::vec3(-1.0, 0.0, 1.0),
        glm::vec3(0.0, -1.0, 0.0)
    };

    std::list<unsigned int> facesList = std::list<unsigned int>{
        0, 1, 2,  0, 2, 3,  0, 3, 4,  0, 4, 1,
        5, 4, 3,  5, 3, 2,  5, 2, 1,  5, 1, 4
    };

    // subdivide
    for(int iter = 0; iter < subdivisions; iter++) {
        int currentSize = facesList.size();
        for(int f = 0; f < currentSize; f += 3) {
            unsigned int i, j, k;
            i = facesList.front();
            facesList.pop_front();
            j = facesList.front();
            facesList.pop_front();
            k = facesList.front();
            facesList.pop_front();

            glm::vec3 newVertices[] = {
                (vertices[i] + vertices[j]) / 2.0f,
                (vertices[j] + vertices[k]) / 2.0f,
                (vertices[k] + vertices[i]) / 2.0f
            };

            for(auto &newVertex : newVertices) {
                vertices.push_back(newVertex);
            }

            int size = vertices.size();
            unsigned int iji = size - 3, jki = size - 2, kii = size - 1;

            unsigned int newFaces[] = {
                i, iji, kii,  j, jki, iji,  k, kii, jki,  iji, jki, kii
            };

            for(auto &newFace : newFaces) {
                facesList.push_back(newFace);
            }
        }
    }

    for(auto &face: facesList) {
        faces.push_back(face);
    }

    // normalize vertices i.o. to create a spherical shape
    for(auto &vertex: vertices) {
        vertex = radius * glm::normalize(vertex) + position;
    }

    vb.emplace(&vertices[0], vertices.size()*3*sizeof(float));
    ib.emplace(&faces[0], faces.size());

    // vertex attributes (position and normal)
    layout.push<float>(3);

    va.addBuffer(*vb, layout);
}

