#include "Grid2D.h"

Grid2D::Grid2D(glm::vec3 position, float width, float height, unsigned int subdivisions) {

    std::vector<unsigned int> &edges = faces;

    for(int i = 0; i <= subdivisions; i++) {
        vertices.emplace_back(-width/2, 0.0, -height/2 + i * height/subdivisions);
        vertices.emplace_back(width/2, 0.0, -height/2 + i * height/subdivisions);
        vertices.emplace_back(-width/2 + i * width/subdivisions, 0.0, -height/2);
        vertices.emplace_back(-width/2 + i * width/subdivisions, 0.0, height/2);
        edges.emplace_back(i * 4 + 0);
        edges.emplace_back(i * 4 + 1);
        edges.emplace_back(i * 4 + 2);
        edges.emplace_back(i * 4 + 3);
    }

    for(auto &vertex: vertices) {
        vertex += position;
    }

    vb.emplace(&vertices[0], vertices.size()*6*sizeof(float));
    ib.emplace(&faces[0], faces.size());

    // vertex attributes (position)
    layout.push<float>(3);

    va.addBuffer(*vb, layout);
    renderType = GL_LINES;
}
