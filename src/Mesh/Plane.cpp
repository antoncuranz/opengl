#include <vector>
#include "Plane.h"

Plane::Plane(glm::vec3 position, float width, float height) {

    vertices = std::vector<glm::vec3>{
        position + glm::vec3(width/2, 0, height/2),
        position + glm::vec3(-width/2, 0, height/2),
        position + glm::vec3(width/2, 0, -height/2),
        position + glm::vec3(-width/2, 0, -height/2),
    };

    faces = std::vector<unsigned int>{
        0, 2, 1, 1, 2, 3
    };

    vb.emplace(&vertices[0], vertices.size()*6*sizeof(float));
    ib.emplace(&faces[0], faces.size());

    // vertex attributes (position)
    layout.push<float>(3);

    va.addBuffer(*vb, layout);
}
