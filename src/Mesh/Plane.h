#ifndef PLANE_H
#define PLANE_H


#include <glm/glm.hpp>
#include "Mesh.h"

class Plane : public Mesh {
public:
    Plane(glm::vec3 position, float width, float height);
};


#endif //PLANE_H
