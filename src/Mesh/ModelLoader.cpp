#include "ModelLoader.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

std::vector<std::shared_ptr<Mesh>> ModelLoader::loadOBJ(const std::string &path) {

    Assimp::Importer importer;

    const aiScene* pScene = importer.ReadFile(path.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_JoinIdenticalVertices);

    std::cout << "[Assimp] File contains " << pScene->mNumMeshes << " meshes and " << pScene->mNumMaterials << " materials." << std::endl;

    std::vector<std::shared_ptr<Mesh>> meshes;
    std::vector<std::shared_ptr<Material>> materials;

    auto textured = std::make_shared<Shader>("../resources/shaders/texture.vert", "../resources/shaders/texture.frag");

    for(int i = 0; i < pScene->mNumMaterials; i++) {
        aiString texture;
        pScene->mMaterials[i]->GetTexture(aiTextureType_DIFFUSE, 0, &texture);
        std::cout << "[Assimp] found texture " << texture.C_Str() << std::endl;

        std::string test("../resources/obj_data/sponza/");
        test.append(texture.C_Str());
        auto textureObj = new Texture(test.c_str());

        auto Textured = std::make_shared<Material>(textured, UF_MVP);
        Textured->addUniform("texture", Uniform { UDT_Sampler2D, textureObj } );

        materials.push_back(Textured);
    }

    for(int i = 0; i < pScene->mNumMeshes; i++) {
        auto mesh = std::make_shared<Mesh>();

        aiMesh *firstMesh = pScene->mMeshes[i];

        for(int j = 0; j < firstMesh->mNumVertices; j++) {
            aiVector3D *pPos = &(firstMesh->mVertices[j]);
            aiVector3D *pNormal = &(firstMesh->mNormals[j]);
            aiVector3D *pTexCoord = &(firstMesh->mTextureCoords[0][j]);

            mesh->vertices.emplace_back(pPos->x, pPos->y, pPos->z);
            mesh->normals.emplace_back(pNormal->x, pNormal->y, pNormal->z);
            mesh->texCoords.emplace_back(pTexCoord->x, pTexCoord->y);
        }

        for(int j = 0; j < firstMesh->mNumFaces ; j++) {
            aiFace& face = firstMesh->mFaces[j];
            mesh->faces.push_back(face.mIndices[0]);
            mesh->faces.push_back(face.mIndices[1]);
            mesh->faces.push_back(face.mIndices[2]);
        }

        mesh->setMaterial(materials[firstMesh->mMaterialIndex]);

        mesh->calculateFaceNormals();
        mesh->calculateVertexNormals(false);
        mesh->calculateVertexArray();

        mesh->vb.emplace(&mesh->vertexArray[0], mesh->vertices.size()*8*sizeof(float));
        mesh->ib.emplace(&mesh->faces[0], mesh->faces.size());

        // vertex attributes
        mesh->layout.push<float>(3); // position
        mesh->layout.push<float>(3); // normal
        mesh->layout.push<float>(2); // texCoords

        mesh->va.addBuffer(*mesh->vb, mesh->layout);

        meshes.push_back(mesh);
    }

    return meshes;
}
