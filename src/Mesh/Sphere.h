#ifndef SPHERE_H
#define SPHERE_H

#include <glm/glm.hpp>
#include <vector>
#include "Mesh.h"

class Sphere : public Mesh {
public:
    explicit Sphere(glm::vec3 position, float radius, int subdivisions);
    explicit Sphere(glm::vec3 position, float radius) : Sphere(position, radius, 4) {};
    explicit Sphere(glm::vec3 position) : Sphere(position, 1.0, 4) {};
};

#endif //SPHERE_H
