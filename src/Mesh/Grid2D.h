#ifndef GRID2D_H
#define GRID2D_H


#include <glm/glm.hpp>
#include "Mesh.h"

class Grid2D : public Mesh {
public:
    Grid2D(glm::vec3 position, float width, float height, unsigned int subdivisions);
};

#endif //GRID2D_H
