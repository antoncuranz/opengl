#ifndef MODELLOADER_H
#define MODELLOADER_H

#include "Mesh.h"

#include <iostream>

class ModelLoader {
public:
    ModelLoader() = default;
    static std::vector<std::shared_ptr<Mesh>> loadOBJ(const std::string &path);
};


#endif //MODELLOADER_H
