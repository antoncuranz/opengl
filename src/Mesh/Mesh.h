#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <list>
#include <optional>
#include "../Core/VertexBuffer.h"
#include "../Core/IndexBuffer.h"
#include "../Core/VertexBufferLayout.h"
#include "../Core/VertexArray.h"
#include "../Scene/Scene.h"

class Mesh : public SceneObject {
public:
    explicit Mesh();
    explicit Mesh(const std::string &path);

    std::vector<unsigned int> faces;
    std::vector<glm::vec3>    vertices;
    std::vector<glm::vec3>    normals;
	std::vector<glm::vec2>    texCoords;
    std::vector<glm::vec3>    faceNormals;

//private:
    std::vector<float> vertexArray;

    void calculateVertexArray();
    void calculateVertexNormals(bool weighted);
    void calculateFaceNormals();
};

#endif //MESH_H
