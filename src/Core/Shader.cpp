#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "Shader.h"
#include "Texture.h"

Shader::Shader(const std::string &vertex, const std::string &fragment, const std::string &geometry)
    : vertexShader(0), fragmentShader(0), geometryShader(0)
{

    vertexShader = compileShader(parseShader(vertex).c_str(), GL_VERTEX_SHADER);
    fragmentShader = compileShader(parseShader(fragment).c_str(), GL_FRAGMENT_SHADER);
    if(!std::empty(geometry))
        geometryShader = compileShader(parseShader(geometry).c_str(), GL_GEOMETRY_SHADER);

    m_RendererID = glCreateProgram();

	std::string test = "Das ist eine neue Zeile";

    glAttachShader(m_RendererID, vertexShader);
    glAttachShader(m_RendererID, fragmentShader);
    if(!std::empty(geometry))
        glAttachShader(m_RendererID, geometryShader);

    glLinkProgram(m_RendererID);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    if(!std::empty(geometry))
        glDeleteShader(geometryShader);
}

std::string Shader::parseShader(const std::string &path) {
    std::ifstream shaderFile;
    std::string shaderCode;

    shaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    try {
        std::stringstream shaderStream;

        shaderFile.open(path);
        shaderStream << shaderFile.rdbuf();
        shaderFile.close();
        shaderCode = shaderStream.str();
    } catch(std::ifstream::failure e) {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }

    return shaderCode;
}

unsigned int Shader::compileShader(const char *shaderSource, int SHADER_TYPE) {
    int success;
    char infoLog[512];

    unsigned int shader = glCreateShader(SHADER_TYPE);
    glShaderSource(shader, 1, &shaderSource, nullptr);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(shader, 512, nullptr, infoLog);
//        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    return shader;
}

void Shader::bind() const {
    glUseProgram(m_RendererID);
}

void Shader::unbind() const {
    glUseProgram(0);
}

void Shader::setUniform(Uniform &uniform, const char *name) const {

    switch(uniform.dataType) {
        case UDT_Vec3:
            glUniform3fv(glGetUniformLocation(m_RendererID, name), 1, (float *)uniform.data);
            break;
        case UDT_Vec4:
            glUniform4fv(glGetUniformLocation(m_RendererID, name), 1, (float *)uniform.data);
            break;
        case UDT_Mat3:
            glUniformMatrix3fv(glGetUniformLocation(m_RendererID, name), 1, GL_FALSE, (float *)uniform.data);
            break;
        case UDT_Mat4:
            glUniformMatrix4fv(glGetUniformLocation(m_RendererID, name), 1, GL_FALSE, (float *)uniform.data);
            break;
        case UDT_Sampler2D:
            ((Texture *)uniform.data)->bind();
            break;
        default:
            break;
    }
//    glUniform1f(glGetUniformLocation(m_RendererID, name), value);
//    glUniform1i(glGetUniformLocation(m_RendererID, name), value);
}
