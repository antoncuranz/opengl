#include "Material.h"

#include <utility>

Material::Material(std::shared_ptr<Shader> shader, char uniformOptions) : m_Shader(std::move(shader)), m_UniformOptions(uniformOptions) {

}

void Material::bind() {

    m_Shader->bind();

    for(const auto &[name, uniform]: m_Uniforms) {
        m_Shader->setUniform((Uniform &)uniform, name.c_str());
    }
}

void Material::addUniform(std::string name, Uniform uniform) {
    m_Uniforms[name] = uniform;
}

void Material::addTexture(std::string &name, Texture &texture, unsigned int index) {

}

