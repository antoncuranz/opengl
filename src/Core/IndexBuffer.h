#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H


class IndexBuffer {
public:
    IndexBuffer(const void *data, unsigned int count);
    ~IndexBuffer();

    void bind() const;
    void unbind() const;

    inline unsigned int getCount() { return m_Count; };
private:
    unsigned int m_RendererID;
    unsigned int m_Count;
};


#endif //INDEXBUFFER_H
