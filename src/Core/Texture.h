#ifndef TEXTURE_H
#define TEXTURE_H


class Texture {
public:
	Texture(const char *path);

	void bind() const;
	void unbind() const;

private:
	unsigned int m_RendererID;
};

#endif //TEXTURE_H
