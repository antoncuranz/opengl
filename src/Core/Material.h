#ifndef MATERIAL_H
#define MATERIAL_H


#include "Texture.h"
#include "Shader.h"
#include "Uniform.h"
#include <glm/glm.hpp>
#include <map>
#include <utility>

/*
 * Material phong(phong_shader, UF_PHONG);
 * phong.addUniform("ambient", Uniform { UDT_Vec3, &colorVec[0] });
 * phong.addTexture("texture", texture, 0);
 *
 * // render
 * bind global uniforms according to options
 * phong.bind();
 */

class Material {
public:
    Material(std::shared_ptr<Shader> shader, char uniformOptions);
    explicit Material(std::shared_ptr<Shader> shader) : Material(std::move(shader), 0x0) {};

    void addUniform(std::string name, Uniform uniform);
    void addTexture(std::string &name, Texture &texture, unsigned int index);

    void bind();

    std::shared_ptr<Shader> m_Shader;
private:
    char m_UniformOptions;
    std::map<std::string, Uniform> m_Uniforms;
    std::vector<Texture> textures;
};


#endif //MATERIAL_H
