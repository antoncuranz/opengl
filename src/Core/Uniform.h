#ifndef UNIFORM_H
#define UNIFORM_H

#include <memory>
#include <glm/glm.hpp>

enum UniformDataType {
    UDT_Int,
    UDT_Float,
    UDT_Vec3,
    UDT_Vec4,
    UDT_Mat3,
    UDT_Mat4,
    UDT_Sampler2D
};

struct Uniform {
//    const char *name;
    int dataType;
    void *data;
};

struct Uniform3v : Uniform {
    Uniform3v() {
        dataType = UDT_Vec3;
    }
};

enum UniformFlag {
    UF_NONE   = 0x0,
    UF_MVP    = 0x1,
    UF_MVIT   = 0x2,
    UF_LIGHTS = 0x4,
    UF_EYE    = 0x8,

    // Shortcuts
    UF_PHONG  = 0x15
};

#endif //UNIFORM_H
