#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <glm/glm.hpp>
#include <vector>
#include "Uniform.h"


class Shader {
public:
    Shader(const std::string &vertex, const std::string &fragment, const std::string &geometry);
    Shader(const std::string &vertex, const std::string &fragment) : Shader(vertex, fragment, "") {};

    // low level access
//    void setUniform(int value, const char *name) const;
//    void setUniform(float value, const char *name) const;
//    void setUniform(glm::mat3 &matrix, const char *name) const;
//    void setUniform(glm::mat4 &matrix, const char *name) const;
//    void setUniform(glm::vec3 &vector, const char *name) const;
//    void setUniform(glm::vec4 &vector, const char *name) const;
    void setUniform(Uniform &uniform, const char *name) const;

    // batch Uniform objects
//    void setUniforms(std::vector<Uniform> &uniforms);

    void bind() const;
    void unbind() const;
private:
    unsigned int m_RendererID;
    unsigned int vertexShader;
    unsigned int fragmentShader;
    unsigned int geometryShader;

    static std::string parseShader(const std::string &path);
    static unsigned int compileShader(const char *shaderSource, int SHADER_TYPE);
};


#endif //SHADER_H
