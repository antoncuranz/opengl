#version 330 core

in vec3 vNormal;
in vec3 vPosition;
in vec2 vTexCoord;

out vec4 FragColor;

uniform vec3 eye;
uniform mat4 model;
uniform mat4 view;

uniform vec3 light;

uniform vec4 Ka;
uniform vec4 Kd;
uniform vec4 Ks;

//uniform sampler2D uTexture;

const float alpha = 10;
vec4 Ca = vec4(0.2, 0.2, 0.2, 0.0);
vec4 Cd = vec4(0.7, 0.2, 0.2, 0.0);
vec4 Cs = vec4(1.0, 1.0, 1.0, 0.0);

vec3 center = vec3(0.0);

void main()
{
    vec3  l    = normalize((view * vec4(light, 1.0)).xyz - vPosition);
    vec3  n    = normalize (vNormal);
//    vec3  n    = normalize(vPosition - (view * model * vec4(center, 1.0)).xyz);
    vec3  r    = reflect(-l, n);
    vec3  v    = normalize(mat3(view * model) * eye - vPosition);
    float diff = 0.75 * max (dot (r, n), 0);
    float spec = 0.4 * pow (max (dot (r, v), 0), alpha);
    FragColor = 0.5 * Ca + diff * Kd + spec * Cs;
//    FragColor = texture(uTexture, vTexCoord);
}
