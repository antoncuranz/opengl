#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;

out vec3 vNormal;
out vec3 vPosition;
out vec2 vTexCoord;

uniform mat4 mvit;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
   vNormal     = normalize(mvit * vec4(aNormal, 0.0)).xyz;
   vPosition   = (view * model * vec4(aPos, 1.0)).xyz;
   vTexCoord   = aTexCoord;
   gl_Position = projection * view * model * vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
