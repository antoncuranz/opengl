#version 330 core

layout(triangles) in;
layout(line_strip, max_vertices=6) out;

in VS_OUT {
    vec3 normal;
} vertex[];

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float normal_length;

void main() {
    mat4 mvp = projection * view * model;

    for(int i=0; i<gl_in.length(); i++) {
        vec3 P = gl_in[i].gl_Position.xyz;
        vec3 N = vertex[i].normal;

        gl_Position = mvp * vec4(P, 1.0);
        EmitVertex();

        gl_Position = mvp * vec4(P + N * normal_length, 1.0);
        EmitVertex();

        EndPrimitive();
    }
}